FROM m4rcu5/lighttpd:staging

LABEL maintainer="Zsibok Mark <zsmark@zsmark.dev>"
LABEL description="My CV website"
LABEL VERSION="1.1"

COPY ./public /var/www/localhost/htdocs
